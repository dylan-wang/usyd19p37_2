# USYD19P37_2

# For deployment
1. Check this video for [details](https://www.youtube.com/watch?v=YS0c3JqD7Sk)
2. For documentation of [instruction and system diagram](https://bitbucket.org/dylan-wang/usyd19p37_2/downloads/Instructions_and_System_diagram.pdf).
3. For [implemented user stories and test cases](https://bitbucket.org/dylan-wang/usyd19p37_2/downloads/deployment_plan.docx).
4. For [deployement assessment](https://bitbucket.org/dylan-wang/usyd19p37_2/downloads/2019_CapstoneProjects_ClientDeployment1_Assessment.pdf).

# Instructions for TimeTracker Stage 3
Please make sure every file are in the same folder.

## Setup backend win32 application ##
1. Download the TimeTracker app from [https://bitbucket.org/dylan-wang/usyd19p37_2/downloads/]( https://bitbucket.org/dylan-wang/usyd19p37_2/downloads/)
2. Run the `Track_Brain_v3.1.msi` installer
- Note: Choose your `Desktop` as installation folder.
4. You now should have a `Time Tracker Release` folder in your Desktop.
5. Run `TimeTracker.exe` to check whether it can run properly. You can see the icon in the taskbar (it might be hidden, and you can show it by clicking the triangle)

    ![7431570453813_.pic.jpg](https://bitbucket.org/repo/RegBnXK/images/4142117587-7431570453813_.pic.jpg)

6. Double click the icon to exit the program.

## Setup native messaging ##
1. Run `init.bat` in your `Time Tracker Release` folder. Then `manifest.json` should be created in the same folder.

![7451570454409_.pic.jpg](https://bitbucket.org/repo/RegBnXK/images/1319182679-7451570454409_.pic.jpg)

2. Go to `Registry Editor` and find `Computer\HKEY_CURRENT_USER\Software\Google\Chrome\NativeMessagingHosts\com.group_project.time_tracker`
 (you can simply copy this into the top input box)

![7441570454409_.pic.jpg](https://bitbucket.org/repo/RegBnXK/images/3446173592-7441570454409_.pic.jpg)  
You need to check whether the path to `manifest.json` is correct. If not, change it.

![7461570454409_.pic.jpg](https://bitbucket.org/repo/RegBnXK/images/3409450862-7461570454409_.pic.jpg)


## Web Server Installation ##

In order to load the web pages, the Web Server for Chrome extension is also needed. If the web server is not set, the web pages won�t be able to load.

1. Open the [Web Server for Chrome extension link](https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb?hl=en), and click `Add to Chrome`.
2. Launch the app and it will show a new window, as shown below.

![7471570455877_.jpg](https://bitbucket.org/repo/RegBnXK/images/2173037197-7471570455877_.jpg)

3. Click `Choose Folder`, and search for the **chrome-extension-folder**(eg. TimeTrackerRelease/Chrome-Extension).
4. The web pages should be able to run properly now.

## Chrome Extension Installation ##

1. Open Chrome and go to `chrome://extensions/`.
2. Enable Developer mode in the right top, and load `Chrome-Extension` in `Time Tracker Release`

![7481570455981_.pic.jpg](https://bitbucket.org/repo/RegBnXK/images/3133525126-7481570455981_.pic.jpg)
	
[Setup Instructions link in wiki](https://bitbucket.org/dylan-wang/usyd19p37/wiki/Instructions%20for%20Deployment)

## Features ##

1. When `Chrome` is opened, the extension will run automatically, and you can find a tray icon at the right bottom. By double clicking it, you can exit the background app.
2. Click the extension icon, and **click the third button** which will lead you to the `dashboard` page.
3. The `dashboard` page will show current goal and related tasks.
4. The `report` page will show daily report and weekly report
5. The `set goal` page allow you to view and set goal.


![7501570467734_.pic.jpg](https://bitbucket.org/repo/RegBnXK/images/244218341-7501570467734_.pic.jpg)