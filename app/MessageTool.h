/*
** This class is used to process the native messaging communication
** between Chrome extension and local application. And this class use
** external library to process JSON files. More details refers to jspn.jpp
**
** To receive message and process it, use getMessage()
**
** To send message to Chrome, use encodeSingleMessage() to encode the message,
** and use sendSingleMsessage() to send the encoded message to the Chrome
**
** 
*/

#pragma once

#include <iostream>
#include <string>
#include "json.hpp"
#include "Database.h"

using namespace std;
using json = nlohmann::json;

struct message_t {
	string content;
	uint32_t length;
};

namespace MessageTool
{

	// Read a message from stdin and decode it.
	json getSingleMessage();

	// Encode a message for transmission, given its content.
	message_t encodeSingleMessage(json content);

	// Send an encoded message to stdout.
	void sendSingleMsessage(message_t encoded_message);

	// Parse message received from chrome
	void getMessage();

	// Request disconnect with native messaging
	void disconnectChrome();


/*

	int main(int argc, char* argv[])
	{
		while (true) {
			json message = get_message();
			if (message == "ping")
				send_message(encode_message("pong"));
		}
	}


*/

};



