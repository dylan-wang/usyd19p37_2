/*
** This class is mainly used to process functions related with
** time calculation
**
** Details of each function can be found at each header
*/


#pragma once

#include "framework.h"

// Temporaty no use
typedef struct USAGE_TIME {
	WORD hour;
	WORD minute;
	WORD second;
} USAGE_TIME, * USAGE_TIME_PTR;

// Calculate seconds given SYSTEMTIME struct,
// only consider 1 day
DWORD getSecond(SYSTEMTIME& time);

// Format given time into hh:mm:ss
std::wstring formatTime(WORD sec);

// Calculate time interval for given SYSTEMTIME struct
// return seconds. Used in Database.h
DWORD subtract(SYSTEMTIME begin, SYSTEMTIME end);

extern void showError(LPCTSTR errorFunc);

// Extract time from string, used for recovering
// app usage from local file
DWORD extractTime(std::wstring usageInfo);

extern void getErrorMessage(LPCTSTR errorFunc);

// This function will be called repeatedly.
// It monitors the current used app by checking the foreground window.
// It also call MessageTool::getMessage() to receive messages from Chrome
void CALLBACK timerFunc(HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime);



