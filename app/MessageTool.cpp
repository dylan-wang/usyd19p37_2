#include "MessageTool.h"

#include <io.h>
#include <fcntl.h>
#include <iostream>
#include <fstream>

void showErrorString(std::string str)
{
	std::wstring_convert< std::codecvt_utf8<wchar_t>, wchar_t> convert;
	showError(convert.from_bytes(str).c_str());
}

json MessageTool::getSingleMessage()
{
	char raw_length[4];
	_setmode(_fileno(stdin), _O_BINARY);
	if (FALSE == fread(raw_length, 4, sizeof(char), stdin))
	{
		return json::object();
	}
	
	int32_t message_length = (((int32_t)raw_length[3]) << 24) | (((int32_t)raw_length[2]) << 16) | (((int32_t)raw_length[1]) << 8) | ((int32_t)raw_length[0]);

	std::vector<char> buffer(message_length);
	fread(&buffer[0], sizeof(char), message_length, stdin);

	std::string msg(buffer.begin(), buffer.end());
	//showErrorString(msg);
	//std::string temp = json::parse(msg)["message"].get<std::string>();

	return json::parse(msg);
}

message_t MessageTool::encodeSingleMessage(json content) {
	string encoded_content = content.dump();
	message_t m;
	m.content = encoded_content;
	m.length = (uint32_t)encoded_content.length();
	return m;
}

void MessageTool::sendSingleMsessage(message_t encoded_message)
{
	char* raw_length = reinterpret_cast<char*>(&encoded_message.length);
	auto res = fwrite(raw_length, sizeof(char), 4, stdout);
	if (res != 4)
	{
		showError(L"raw_length cannot write into stdout");
	}

	res = fwrite(encoded_message.content.c_str(), sizeof(char), encoded_message.length, stdout);
	if (res != encoded_message.length)
	{
		showError(L"encoded message cannot write into stdout");
	}

	fflush(stdout);
}

/* Receive format: {\"command:\": some command,\"args\": given arguments(a list)}
** JSON.stringify({command: "add goal", args: "goalName; task1,<,2; task2,>,3"})
*/
void MessageTool::getMessage()
{
	
	std::wstring_convert< std::codecvt_utf8<wchar_t>, wchar_t> convert;

	json m = getSingleMessage();

	if (m.empty())
	{
		
		return;
	}
	
	// Extract command
	std::string command = m["command"].get<std::string>();

	// Extract args
	std::string arguments = m["args"].get<std::string>();

	json data;
	data["msg"] = "native app";

	if (command.compare("get app usage") == 0)
	{
		// Get dayly report data from Database
		APP_IT_PAIR appList = Database::getInstance().getCurrentUsage();

		std::vector<std::string> appName;
		std::vector<int> appUsage;

		// Extract info
		for (auto i = appList.first; i != appList.second; i++)
		{
			appName.push_back(convert.to_bytes(i->first));
			appUsage.push_back(i->second.usage);		
		}

		data["appName"] = appName;
		data["appUsage"] = appUsage;
	}
	else if (command.compare("quit") == 0)
	{
		disconnectChrome();
		PostQuitMessage(0);
	}
	else
	{
		data["msg"] = "invalid message";
	}
	
	sendSingleMsessage(encodeSingleMessage(data));
	
}

void MessageTool::disconnectChrome()
{
	json data;
	data["msg"] = "disconnect";
	sendSingleMsessage(encodeSingleMessage(data));
}