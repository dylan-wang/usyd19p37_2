//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TimeTracker.rc
//
#define IDC_MYICON                      2
#define IDD_TIMETRACKER_DIALOG          102
#define IDD_VIEW                        102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDD_CATEGORIZE                  104
#define IDM_EXIT                        105
#define IDI_TIMETRACKER                 107
#define IDI_SMALL                       108
#define IDC_TIMETRACKER                 109
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     130
#define IDD_SET_GOAL                    131
#define IDD_ADD_TASK                    132
#define IDD_VIEW_GOAL                   133
#define IDD_REPORT                      134
#define IDD_WEEKLYREPORT                134
#define IDD_DETAILEDWEEKLY              135
#define IDD_DAILYREPORT                 136
#define IDD_DETAILEDDAILY               137
#define IDD_ADD_GOAL                    138
#define IDC_LIST1_VIEW                  1000
#define IDC_DATETIMEPICKER1             1003
#define IDC_DATETIMEPICKER_VIEW         1003
#define IDC_TEXT_DATE                   1007
#define IDC_LIST1                       1008
#define IDC_RADIO1                      1009
#define IDC_RADIO2                      1010
#define IDC_RADIO3                      1011
#define IDC_RADIO4                      1012
#define IDC_BUTTON1                     1013
#define IDC_REMOVE                      1013
#define IDC_LIST2                       1017
#define IDC_COMBO1                      1018
#define IDC_SLIDER1                     1019
#define IDC_COMBO2                      1020
#define IDC_COMBO3                      1021
#define IDC_RADIO5                      1022
#define IDC_RADIO6                      1023
#define IDC_GOAL_TEXT                   1024
#define IDC_LIST3                       1025
#define IDC_WEEKLYREPORT_DETAILS        1031
#define IDC_DAILYREPORT_DETAILS         1032
#define IDC_DETAILEDDAILY_LIST          1034
#define IDC_DR_COMBO                    1035
#define IDC_EDIT1                       1036
#define IDC_BUTTON2                     1037
#define IDC_COMBO1_DETAIL               1039
#define ID_HOME_VIEWYOURACTIVITIES      32771
#define ID_GOALS                        32772
#define ID_GOALS_SETGOALS               32773
#define ID_SETTINGS_CATEGORIZEYOURACTIVITIES 32774
#define ID_SETTINGS_STOPTRACKING        32775
#define ID_GOAL_VIEWGOAL                32776
#define ID_HOME_EXIT                    32777
#define IDM_HOME_VIEWREPORT             32778
#define ID_HOME_VIEWYOURREPORT          32783
#define ID_VIEWYOURREPORT_WEEKLYREPORT  32784
#define ID_VIEWYOURREPORT_DAILYREPORT   32785
#define ID_VIEWYOURREPORT_WEEKLYREPORT32786 32786
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        139
#define _APS_NEXT_COMMAND_VALUE         32787
#define _APS_NEXT_CONTROL_VALUE         1040
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
