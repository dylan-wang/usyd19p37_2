// header.h : include file for standard system include files,
// or project specific include files
//

#pragma once

#include "targetver.h"
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files
#include <windows.h>
#include <shellapi.h>
#include <ShlObj.h>
// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <string>
#include <fstream>
#include <sstream>
#include <strsafe.h>
#include <regex>
#include <codecvt>
#include <locale>
#include <psapi.h>
#include <stdio.h>
#include <iostream>
#include "Shlwapi.h"
#include "tchar.h"
