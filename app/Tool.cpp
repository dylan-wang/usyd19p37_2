#include "Tool.h"

//For test
VOID sendMessageToConsole(LPCTSTR funcName, LPCTSTR message) {
	OutputDebugString(L".......");
	OutputDebugString(funcName);
	OutputDebugString(L".......\n");
	OutputDebugString(message);
	OutputDebugString(L"\n.......\n");
}

void showError(LPCTSTR msg)
{
	MessageBox(NULL, msg, L"Time Tracker", MB_OK);
}

/*
** Get the error message, dispglay it and exit the process.
** errorFunc: name of the failed process
** msgBuf: error message retrived from the error code
** displayBuf: error message to be displayed
*/
void getErrorMessage(LPCTSTR errorFunc)
{
	LPVOID msgBuf;
	LPVOID displayBuf;
	DWORD error = GetLastError();

	//If there is no error message
	if (error == 0)
		return;

	//Retrive the error message
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		error,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPWSTR)& msgBuf,
		0, NULL);

	displayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
		(lstrlen((LPCTSTR)msgBuf) + lstrlen((LPCTSTR)errorFunc) + 40) * sizeof(TCHAR));

	StringCchPrintf((LPTSTR)displayBuf,
		LocalSize(displayBuf) / sizeof(TCHAR),
		TEXT("%s failed. Error %d: %s"),
		errorFunc,
		error,
		msgBuf); //TODO: warning here
	showError((LPCTSTR)displayBuf);

	LocalFree(msgBuf);
	LocalFree(displayBuf);
	ExitProcess(error);
}

//Record the num of bytes that has been loaded
DWORD bytesLoaded;

//For I/O operation (completed/cancelled)
VOID CALLBACK fileRoutine(DWORD dwErrorCode, DWORD dwNumberOfBytesTransfered, LPOVERLAPPED lpOverlapped)
{
	bytesLoaded = dwNumberOfBytesTransfered;
	//Error: I/O operation was completed successfully
	if (dwErrorCode != 0)
	{
		getErrorMessage(L"FileIOCompletionRoutine");
	}
}


BOOL folderExists(LPCTSTR folder) {
	DWORD attr = GetFileAttributes(folder);
	return ((attr != INVALID_FILE_ATTRIBUTES) && (attr & FILE_ATTRIBUTE_DIRECTORY));
}

BOOL fileExists(LPCTSTR fileName) {
	DWORD attr = GetFileAttributes(fileName);
	return ((attr != INVALID_FILE_ATTRIBUTES) && !(attr & FILE_ATTRIBUTE_DIRECTORY));
}

std::wstring extractDataFolderPath() {
	//Generate default path
	WCHAR path[MAX_PATH];
	if (0 == GetCurrentDirectory(MAX_PATH, path)) {
		getErrorMessage(L"GetCurrentDirectory");
	}

	//Append the path with the folder "data"
	std::wstring filePath;
	filePath.append(path).append(L"\\data\\");

	//Make a data folder
	if (!fileExists(filePath.c_str())) {
		CreateDirectory(filePath.c_str(), NULL);
	}

	return filePath;
}

std::wstring generateFilePath(std::wstring& path, SYSTEMTIME st)
{

	//Set the file name according to the date
	WCHAR tempName[MAX_PATH];
	if (GetDateFormat(LOCALE_USER_DEFAULT, 0, &st, L"dd-MM-yy", tempName, sizeof(tempName) / sizeof(WCHAR)) == 0)
	{
		getErrorMessage(L"SetFileName");
		return L"";
	}

	//Append the full path into the file name
	std::wstring fileName = path + std::wstring(tempName) + L".dat";

	return fileName;
}


//Check which file to be loaed or saved
std::wstring fileNameDetector(std::wstring& path, DWORD mode, SYSTEMTIME st)
{
	switch (mode) {
	case DAILY_USAGE:
		SYSTEMTIME lt;
		GetLocalTime(&lt);
		return generateFilePath(path, lt);
	case WEEKLY_USAGE:
		return generateFilePath(path, st);
	case CATEGORY_DATA:
		return path + L"category.dat";
	case GOALS_DATA:
		return path + L"goals.dat";
	case GOALS_ACHIEVEMENT:
		return path + L"goals_achieve.dat";

	}

	return L"invalid file name";
}



//Load data from local file
std::wstring loadData(std::wstring& path, DWORD mode, SYSTEMTIME st)
{

	//Generate the full path of file
	std::wstring fileName = fileNameDetector(path, mode, st);

	//No local file to be loaded
	if (FALSE == fileExists(fileName.c_str())) {
		return L"";
	}

	//Create the file to read data
	HANDLE file = INVALID_HANDLE_VALUE;
	WCHAR buffer[MAX_BUFFER_SIZE] = { 0 };
	OVERLAPPED o = { 0 };
	bytesLoaded = 0;

	file = CreateFile(fileName.c_str(),
		GENERIC_READ,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,
		NULL);

	//Error: If fail to open the file
	if (file == INVALID_HANDLE_VALUE) {
		getErrorMessage(L"OpenFile");
		return L"";
	}

	//Error: If fail to read file
	if (FALSE == ReadFileEx(file, buffer, MAX_BUFFER_SIZE - 1, &o, fileRoutine))
	{
		getErrorMessage(L"ReadFile");
		CloseHandle(file);
		return L"";
	}
	//Time out
	SleepEx(TIME_OUT, TRUE);

	if (bytesLoaded <= MAX_BUFFER_SIZE - 1)
	{
		buffer[bytesLoaded] = '\0';
	}
	else
	{
		showError(L"Unexpected data loaded.");
	}

	CloseHandle(file);

	std::wstring data = buffer;

	OutputDebugString(std::wstring(L"From ").append(std::to_wstring(mode)).c_str());
	sendMessageToConsole(L"loadData", buffer);

	return data;

}

/*
** Write data into file in directory data
** buffer: data need to be written
** size:   bytes of data to be written
*/
void saveData(std::wstring& path, LPCVOID buffer, DWORD size, DWORD mode)
{
	SYSTEMTIME lt;
	GetLocalTime(&lt);

	//Generate the full path of file
	std::wstring fileName = fileNameDetector(path, mode);

	//Create the file to store data
	HANDLE file = INVALID_HANDLE_VALUE;
	DWORD bufferWritten = 0;
	OVERLAPPED o = { 0 };

	std::wstring bufferData((LPCWSTR)buffer);
	if (mode == GOALS_ACHIEVEMENT)
	{
		bufferData += loadData(path, GOALS_ACHIEVEMENT);
		buffer = (LPCVOID)bufferData.c_str();
		size = (DWORD)wcslen((LPCWSTR)buffer) * 2;

	}

	//Test
	sendMessageToConsole(L"saveData", (LPCTSTR)buffer);


	file = CreateFile(fileName.c_str(),
		GENERIC_WRITE,
		0,
		NULL,
		CREATE_ALWAYS, //Create a new file. If the file exsists, overwrite it.
		FILE_ATTRIBUTE_NORMAL,
		NULL);


	//Error: If fail to create new file
	if (file == INVALID_HANDLE_VALUE) {
		getErrorMessage(L"CreateNewFile");
		return;
	}

	//Error: If fail to write file
	if (FALSE == WriteFile(file, buffer, size, &bufferWritten, &o)) {
		getErrorMessage(L"WriteFile");
		return;
	}
	else {
		//Error: If the data has been written imcompletely
		if (bufferWritten != size) {
			WCHAR errorMesssage[MAX_LEN];
			swprintf_s(errorMesssage, MAX_LEN, _T("Note: Imcomplete Data!\nOnly %d bytes of data has been written."), bufferWritten);
			showError(errorMesssage);
		}
		else
		{
			//Test Only
			/*
			OutputDebugString(L".......\n");
			OutputDebugString(L"Data has been written completely into the file");
			OutputDebugString(L"\n.......\n");
			*/
			//_tprintf(TEXT("Data has been written completely into the file: %s\n"), path);
		}

	}

	CloseHandle(file);

}



// SYSTEMTIME arithmetic
// TODO: test for accuracy
SYSTEMTIME lastday(SYSTEMTIME& date)
{
	SYSTEMTIME res(date);

	if (date.wDay != 1)
	{
		res.wDay -= 1;
		return res;
	}

	if (date.wMonth == 5 || date.wMonth == 7 || date.wMonth == 10 || date.wMonth == 12)
	{
		date.wMonth--;
		date.wDay = 30;
	}
	else if (date.wMonth == 2 || date.wMonth == 4 || date.wMonth == 6 ||
		date.wMonth == 8 || date.wMonth == 9 || date.wMonth == 11)
	{
		date.wMonth--;
		date.wDay = 31;
	}
	else if (date.wMonth == 3)
	{
		int day = 0;

		if (date.wYear % 400 == 0 || (date.wYear % 100 != 0 && date.wYear % 4 == 0))
		{
			day = 29;
		}
		else
		{
			day = 28;
		}

		date.wMonth--;
		date.wDay = day;
	}
	else if (date.wMonth == 1)
	{
		date.wYear--;
		date.wMonth = 12;
		date.wDay = 31;
	}

	return res;
}

std::wstring loadWeeklyRecord()
{
	SYSTEMTIME lt;
	GetLocalTime(&lt);

	std::wstring data;
	std::wstring path = extractDataFolderPath();
	for (int i = 0; i < 7; i++) {
		lt = lastday(lt);

		data += loadData(path, WEEKLY_USAGE, lt);

	}

	return data;

}

