/*
** This class includes helper functions for debugging,
** processing files
**
*/



#pragma once

#include "framework.h"

#define MAX_LEN 100
#define MAX_BUFFER_SIZE 2056
#define TIME_OUT 5000
#define DAILY_USAGE 0
#define WEEKLY_USAGE 1
#define CATEGORY_DATA 2
#define GOALS_DATA 3
#define GOALS_ACHIEVEMENT 4

//For debug
VOID sendMessageToConsole(LPCTSTR funcName, LPCTSTR message);

// For debug
void showError(LPCTSTR msg);

/*
** Get the error message, dispglay it and exit the process.
** errorFunc: name of the failed process
** msgBuf: error message retrived from the error code
** displayBuf: error message to be displayed
*/
void getErrorMessage(LPCTSTR errorFunc);

// For I/O operation (completed/cancelled)
VOID CALLBACK fileRoutine(DWORD dwErrorCode, DWORD dwNumberOfBytesTransfered, LPOVERLAPPED lpOverlapped);

// Check whether the folder exists
BOOL folderExists(LPCTSTR folder);

// Check whether the file exists
BOOL fileExists(LPCTSTR fileName);

// Extract and create a path to
// store local files in the same folder
std::wstring extractDataFolderPath();

// Generate path for file to store based on the date
std::wstring generateFilePath(std::wstring& path, SYSTEMTIME st);

//Check which file to be loaed or saved
std::wstring fileNameDetector(std::wstring& path, DWORD mode, SYSTEMTIME st = SYSTEMTIME());

//Load data from local file
std::wstring loadData(std::wstring& path, DWORD mode, SYSTEMTIME st = SYSTEMTIME());

/*
** Write data into file in directory data
** buffer: data need to be written
** size:   bytes of data to be written
*/
void saveData(std::wstring& path, LPCVOID buffer, DWORD size, DWORD mode);


// SYSTEMTIME arithmetic
// TODO: test for accuracy
SYSTEMTIME lastday(SYSTEMTIME& date);

// No use
std::wstring loadWeeklyRecord();

