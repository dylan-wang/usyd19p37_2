# Overview

This folder contains coding of native Window desktop app.

- [Overview](#overview)
- [Core function](#core-function)
- [Communication with Chrome](#communication-with-chrome)
- [Monitor app usage](#monitor-app-usage)
- [Utilities](#utilities)

# Core function

`Database.h`, `Database.cpp`

This class is used to store the recorded app usage.

To use it, you can simply call `Database::getInstance()` which is implemented by Singleton Pattern.

The app usage time is calculated by `updateAppUsageTime()`

When the program is terminating, it will save data into loacal file by `printUsageResult()`

When the program is reopend, it will reload current day's file by `recoverAppUsageTime`

To retrieve app usage, use `getCurrentUsage()` to return you an iterator to loop the database.

# Communication with Chrome

`MessageTool.h`, `MessageTool.cpp`

This class is used to process the native messaging communication between Chrome extension and local application. And this class use
external library to process JSON files. More details refers to `jspn.jpp`

To receive message and process it, use `getMessage()`

To send message to Chrome, use `encodeSingleMessage()` to encode the message,
and use `sendSingleMsessage()` to send the encoded message to the Chrome

# Monitor app usage

`TimeCalculator.h`, `TimeCalculator.cpp`

This class contains codes about how to monitor current user activities. Also, it provides functions to dealing with time calculation.

# Utilities

`Tool.h`, `Tool.cpp`

This class contains helper functions including dealing with file stream system and debug functions.