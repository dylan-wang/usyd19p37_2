#include "Database.h"

Database::Database()
{
	if (FALSE == recoverAppUsageTime()) {
		showError(L"Error occurs when recover data from local file in Database class. (Database.h)");
	}
}

Database::~Database()
{
	printUsageResult();
}


BOOL Database::addNewApp(const std::wstring& name, SYSTEMTIME start)
{
	if (checkExistApp(name))
	{
		appList.at(name).start = start;
		appList.at(name).end = start;
		return TRUE;
	}

	APP_INFO info;
	info.name = name;
	info.start = start;
	info.end = start;
	info.usage = 0;

	auto res = appList.insert({ name, info });
	return res.second;
}

BOOL Database::initAppInfo(const std::wstring& name, const std::wstring& usageInfo)
{
	if (checkExistApp(name))
	{
		return TRUE;
	}

	APP_INFO info;
	info.name = name;
	info.usage = extractTime(usageInfo);


	//Test Only
	/*
	sendMessageToConsole(L"InitAppInfo", name.c_str());
	WCHAR time[100];
	swprintf_s(time, 100, L"%d", info.usage);
	std::wstring s(time);
	sendMessageToConsole(L"InitAppInfo", s.c_str());
	*/

	auto res = appList.insert({ name, info });
	return res.second;
}

BOOL Database::checkExistApp(const std::wstring& name) const
{
	return !(appList.find(name) == appList.end());
}

BOOL Database::deleteApp(const std::wstring& name)
{
	if (!checkExistApp(name))
	{
		return TRUE;
	}

	appList.erase(appList.find(name));
	return TRUE;
}

BOOL Database::updateAppStart(const std::wstring& name, SYSTEMTIME start)
{
	if (!checkExistApp(name))
	{
		showError(L"cannot update start: app not in database. (Database.h)");
		return FALSE;
	}

	appList.at(name).start = start;
	appList.at(name).end = start;
	return TRUE;
}

BOOL Database::updateAppEnd(const std::wstring& name, SYSTEMTIME end)
{
	if (!checkExistApp(name))
	{
		showError(L"cannot update end: app not in database. (Database.h)");
		return FALSE;
	}

	appList.at(name).end = end;
	updateAppUsageTime(name);
	return TRUE;

	if (!checkExistApp(name))
	{
		showError(L"cannot update app usage: not in database. (Database.h)");
		return FALSE;
	}

	auto& info = appList.at(name);
	DWORD time = subtract(info.start, info.end);
	info.usage += time;

	return TRUE;
}

BOOL Database::updateAppUsageTime(const std::wstring& name)
{
	if (!checkExistApp(name))
	{
		showError(L"cannot update app usage: not in database. (Database.h)");
		return FALSE;
	}

	auto& info = appList.at(name);
	DWORD time = subtract(info.start, info.end);
	info.usage += time;

	return TRUE;
}


BOOL Database::recoverAppUsageTime()
{
	//Load data from default folder
	std::wstring path = extractDataFolderPath();
	std::stringstream data;
	std::wstring loadingData = loadData(path, 0);

	//If no local data to be loaded
	if (loadingData.empty()) {
		return TRUE;
	}

	data << std::string(loadingData.begin(), loadingData.end());

	std::string buffer;
	while (std::getline(data, buffer) && data.rdbuf()->in_avail() != 0) {
		std::regex regex("-");
		std::vector<std::string> temp(std::sregex_token_iterator(buffer.begin(),
			buffer.end(),
			regex, -1),
			std::sregex_token_iterator());

		if (temp.size() == 2)
		{
			std::wstring_convert< std::codecvt_utf8<wchar_t>, wchar_t> convert;
			if (FALSE == initAppInfo(convert.from_bytes(temp.at(0)), convert.from_bytes(temp.at(1))))
			{
				showError(L"Error: Cannot load application information. (Database.h)");
				return FALSE;
			}
		}
		else
		{
			showError(L"Error: False data has been loaded. (Database.h)");
			return FALSE;
		}
	}

	return TRUE;


}

DWORD Database::getAppTime(const std::wstring& name) const
{
	if (!checkExistApp(name))
	{
		showError(L"cannot get app usage: not in database. (Database.h)");
		return FALSE;
	}

	return appList.at(name).usage;
}

std::wstring Database::getFormatAppTime(const std::wstring& name) const
{
	return formatTime(appList.at(name).usage);
}

APP_IT_PAIR Database::getCurrentUsage() const
{
	APP_IT_PAIR p(appList.cbegin(), appList.cend());
	return p;
}

/*
** This func should call saveData() in Tool.h
** File need be stored in a directory named data
** data: contains the usage time of each app
*/
void Database::printUsageResult() const
{
	//Temp data to be stored
	std::wstring temp;

	//Record format: appname : usage time
	for (auto const& key : appList)
	{
		temp += key.first + L"-" + getFormatAppTime(key.first) + L'\n';
	}

	std::wstring path = extractDataFolderPath();

	//Convert the buffer into LPCWSTR
	LPCWSTR data = temp.c_str();

	//Save data using the function saveData
	saveData(path, (LPCVOID)data, (DWORD)wcslen(data) * 2, 0);
}


