/*
** This class is used to store the recorded app usage.
** To use it, you can simply call Database::getInstance() which
** is implemented by Singleton Pattern.
**
** The app usage time is calculated by updateAppUsageTime()
**
** When the program is terminating, it will save data into loacal file
** by printUsageResult()
**
** When the program is reopend, it will reload current day's file by 
** recoverAppUsageTime
**
** To retrieve app usage, use getCurrentUsage() to return you an iterator 
** to loop the database.
*/

#pragma once

#include <map>
#include <list>

#include "framework.h"
#include "Tool.h"
#include "TimeCalculator.h"


/*
** Store app info including app name, last use and close time,
** and app usage time
*/
typedef struct APP_INFO
{
	std::wstring name = L"";		      // Record app name
	SYSTEMTIME start = SYSTEMTIME();	  // Record time of starting using foreground window
	SYSTEMTIME end = SYSTEMTIME();		  // Record time of stopping using foreground window
	DWORD usage = 0;					  // Record app usage time by secs
}APP_INFO, * APP_INFO_PTR;

typedef std::map<std::wstring, APP_INFO> APP_MAP;

typedef std::pair<APP_MAP::const_iterator, APP_MAP::const_iterator> APP_IT_PAIR;


class Database
{
public:
	~Database();
	Database(Database& other) = delete;
	Database& operator=(Database& other) = delete;

	BOOL addNewApp(const std::wstring& name, SYSTEMTIME start);

	//Initialize App information
	BOOL initAppInfo(const std::wstring& name, const std::wstring& usageInfo);

	BOOL checkExistApp(const std::wstring& name) const;

	BOOL deleteApp(const std::wstring& name);

	// Update app start time and set end to start
	BOOL updateAppStart(const std::wstring& name, SYSTEMTIME start);

	// Update app end time and update usage time
	BOOL updateAppEnd(const std::wstring& name, SYSTEMTIME end);

	// Update app usage time
	BOOL updateAppUsageTime(const std::wstring& name);

	//Recover app usage time from local file
	BOOL recoverAppUsageTime();

	// Return app usage time
	DWORD getAppTime(const std::wstring& name) const;

	// Return format app usage time 
	std::wstring getFormatAppTime(const std::wstring& name) const;

	// Save current app usage for displaying
	APP_IT_PAIR getCurrentUsage() const;

	// Save record data into file
	void printUsageResult() const;

	// Singleton
	static Database& getInstance()
	{
		static Database instance;
		return instance;
	}

private:
	Database();
	APP_MAP appList;
};