#include "TimeCalculator.h"

#include "Database.h"
#include "MessageTool.h"

const int minuteFactor = 60;
const int hourFactor = 3600;
const int dayFactor = 86400;

// Calculate seconds given system time,
// only consider 1 day
DWORD getSecond(SYSTEMTIME& time)
{
	return time.wHour * hourFactor + time.wMinute * minuteFactor + time.wSecond;
}

std::wstring formatTime(WORD sec)
{
	WORD hour = sec / hourFactor;
	WORD minute = (sec % hourFactor) / minuteFactor;
	WORD second = sec % minuteFactor;

	WCHAR time[100];
	swprintf_s(time, 100, L"%d:%d:%d", hour, minute, second);
	std::wstring s(time);
	return s;
}


// Calculate time interval
// return seconds
DWORD subtract(SYSTEMTIME begin, SYSTEMTIME end)
{
	return getSecond(end) - getSecond(begin);
}

extern void showError(LPCTSTR errorFunc);

//Extract time from string
DWORD extractTime(std::wstring usageInfo)
{
	DWORD usage = 0;
	std::wstring_convert< std::codecvt_utf8<wchar_t>, wchar_t> convert;
	std::string temp = convert.to_bytes(usageInfo);

	std::regex regex(":");
	std::vector<std::string> info(std::sregex_token_iterator(temp.begin(),
		temp.end(),
		regex, -1),
		std::sregex_token_iterator());

	if (info.size() == 3)
	{
		DWORD hr = std::stoul(info.at(0));
		DWORD min = std::stoul(info.at(1));
		DWORD sec = std::stoul(info.at(2));
		usage = hr * hourFactor + min * minuteFactor + sec;
	}
	else
	{
		showError(L"Error: Fail to extract usage time from file.");
		return FALSE;
	}

	return usage;
}

extern void getErrorMessage(LPCTSTR errorFunc);

std::wstring lastForegroundWin = L"null window";

// From https://docs.microsoft.com/zh-cn/windows/win32/secauthz/enabling-and-disabling-privileges-in-c--
BOOL SetPrivilege(
	HANDLE hToken,          // access token handle
	LPCTSTR lpszPrivilege,  // name of privilege to enable/disable
	BOOL bEnablePrivilege   // to enable or disable privilege
)
{
	TOKEN_PRIVILEGES tp;
	LUID luid;

	if (!LookupPrivilegeValue(
		NULL,            // lookup privilege on local system
		lpszPrivilege,   // privilege to lookup 
		&luid))        // receives LUID of privilege
	{
		printf("LookupPrivilegeValue error: %u\n", GetLastError());
		return FALSE;
	}

	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = luid;
	if (bEnablePrivilege)
		tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	else
		tp.Privileges[0].Attributes = 0;

	// Enable the privilege or disable all privileges.

	if (!AdjustTokenPrivileges(
		hToken,
		FALSE,
		&tp,
		sizeof(TOKEN_PRIVILEGES),
		(PTOKEN_PRIVILEGES)NULL,
		(PDWORD)NULL))
	{
		printf("AdjustTokenPrivileges error: %u\n", GetLastError());
		return FALSE;
	}

	if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)

	{
		printf("The token does not have the specified privilege. \n");
		return FALSE;
	}

	return TRUE;
}

BOOL CALLBACK EnumChildWindowsFunc(HWND hWnd, LPARAM lp) {
	DWORD* pids = (DWORD*)lp;
	DWORD pid = 0;
	GetWindowThreadProcessId(hWnd, &pid);
	if (pid != pids[0])
	{
		pids[1] = pid;
	}

	return TRUE;
}

void CALLBACK timerFunc(HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime)
{


	Database& db = Database::getInstance();	

	//Json File
	MessageTool::getMessage();
	
	HWND foregroundWin = GetForegroundWindow();
	SYSTEMTIME time;
	DWORD pids[] = { 0, 0 };
	WCHAR exename[100];

	if (foregroundWin)
	{
		GetSystemTime(&time);

		
		GetWindowThreadProcessId(foregroundWin, pids);
		pids[1] = pids[0];

		EnumChildWindows(foregroundWin, EnumChildWindowsFunc, (LPARAM)pids);

		HANDLE processHandle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
			FALSE, pids[1]);

		if (processHandle)
		{
			if (GetModuleFileNameExW(processHandle, NULL, exename, 100))
			{
				PWCHAR start = wcsrchr(exename, L'\\');
				if (start) wmemmove(exename, start + 1, wcslen(start));

				PWCHAR end = wcsrchr(exename, L'.');
				if (end)* end = L'\0';

				for (PWCHAR p = exename; *p; p++)* p = tolower(*p);

				std::wstring appName(exename);

				if (lastForegroundWin.compare(L"null window") == 0) // First window since tracker runs
				{
					db.addNewApp(appName, time);
				}
				else
				{
					if (appName != lastForegroundWin) // User changes window
					{
						db.updateAppEnd(lastForegroundWin, time); // Update and calculate last window usage

						if (!db.checkExistApp(appName)) // Newly opened app
						{
							db.addNewApp(appName, time);
						}
						else
						{
							db.updateAppStart(appName, time);
						}
					}
					else // Prevent users close window directly without saving
					{
						db.updateAppEnd(appName, time);
						db.updateAppStart(appName, time);
					}
				}

				lastForegroundWin = appName;

			}
			else
			{
				getErrorMessage(L"GetModuleFileNameEx");
			}

			CloseHandle(processHandle);
		}
		else
		{
			//getErrorMessage(L"OpenCurrentProcess");
		}


	}
}
