/*
** This file contains operations for indexDB including:
** create DB: openDB()
** get methods: getObjectStore()
** clear store methods: clearStore(), clearUsageStore(), clearGoalStore()
** set current goal: setCurrentGoal()
** add new app into DB: addApp()
** update exist app data: updateApp()
** add new goal into DB: addGoal()
** generate different colour for chart: colorGenerator()
**
** It also contains the communication with view html page and content js which
** receives messages from local app:
** process local app message: receiveMessage(), parseNativeAppMessage()
**
** communicate with html page: addEventListeners()
** modify html page: generateDailyReportPage(), generateWeeklyReportPage(), generateGoalAddingPage()
** 					 generateGoalPage(), generateDashboardPage()
**
*/

$(function () {

    // Define our indexedDb info
    const DB_NAME = 'app-database';
    const DB_VERSION = 1; // Use a long long for this value (don't use a float)
    const USAGE_DB_STORE_NAME = 'usages';
    const GOAL_DB_STORE_NAME = 'goals';
    const MIXED_CATEGORY_STORE_NAME = 'mixed_category';

    var db;

    // Lode all values into the local valriable
    var databaseInfo =
    {
        categories: undefined,
        dailyCategories: undefined,
        dailyCategoryUsages: undefined,
        appList: undefined,
        weeklyCategories: undefined,
        weeklyUsages: undefined,
        goals: undefined,
        tasks: undefined,
        currentGoal: undefined,
        allAppWithCategories: undefined,
        date: undefined,
        mixedCategory: undefined
    }

    // Open our indexedDB
    function openDb() {
        console.log("openAppDataBase ...");
        var req = indexedDB.open(DB_NAME, DB_VERSION);

        //Once all processes have been executed successfullly.
        req.onsuccess = function (evt) {
            // Equal to: db = req.result;
            db = this.result;
            // addDummyData();
            console.log("open Db DONE");
            buildPages();
        };
        req.onerror = function (evt) {
            console.error("openDb:", evt.target.errorCode);
        };

        req.onupgradeneeded = function (evt) {
            // evt.currentTarget.result.deleteObjectStore(USAGE_DB_STORE_NAME);
            // evt.currentTarget.rgeesult.deleteObjectStore(GOAL_DB_STORE_NAME);

            // Setup schemas for each object store.
            console.log("usage.onupgradeneeded");
            var uage_store = evt.currentTarget.result.createObjectStore(
                USAGE_DB_STORE_NAME, { keyPath: 'id', autoIncrement: true });
            uage_store.createIndex('appName', 'appName', { unique: false });
            uage_store.createIndex('appUsage', 'appUsage', { unique: false });
            uage_store.createIndex('appCategory', 'appCategory', { unique: false });
            uage_store.createIndex('date', 'date', { unique: false });

            var goal_store = evt.currentTarget.result.createObjectStore(
                GOAL_DB_STORE_NAME, { keyPath: 'id', autoIncrement: true });
            goal_store.createIndex('goalName', 'goalName', { unique: false });
            goal_store.createIndex('description', 'description', { unique: false });
            goal_store.createIndex('noMoreThan', 'noMoreThan', { unique: false });
            goal_store.createIndex('duration', 'duration', { unique: false });
            goal_store.createIndex('currentGoal', 'currentGoal', { unque: false });

            var mixed_category_store = evt.currentTarget.result.createObjectStore(
                MIXED_CATEGORY_STORE_NAME, { keyPath: 'appName' });
            mixed_category_store.createIndex('appName', 'appName', { unique: true });
            mixed_category_store.createIndex('highlyProductive', 'highlyProductive', { unique: false });
            mixed_category_store.createIndex('productive', 'productive', { unique: false });
            mixed_category_store.createIndex('medium', 'medium', { unique: false });
            mixed_category_store.createIndex('distractive', 'distractive', { unique: false });
            mixed_category_store.createIndex('highlyDistractive', 'highlyDistractive', { unique: false });
        };
    }

    function buildPages() {
        getAllCategories();

        // Hide categorise page popup table
        if ($('#mixed-category-ratio').length) {
            $('#mixed-category-ratio').hide();
        }
    }


	/**
	 * @param {string} store_name
	 * @param {string} mode either "readonly" or "readwrite"
	 */
    function getObjectStore(store_name, mode) {
        var tx = db.transaction([store_name], mode);
        return tx.objectStore(store_name);
    }

    // Clear the object store
    function clearStore(store) {
        var req = store.clear();
        req.onsuccess = function (evt) {
            alert("Store cleared");
        };
        //Error handling
        req.onerror = function (evt) {
            console.error("clearStore:", evt.target.errorCode);
            alert(this.error);
        };
    }

    // Clear usage object sotre explicitly
    function clearUsageStore() {
        var store = getObjectStore(USAGE_DB_STORE_NAME, 'readwrite');
        clearStore(store);

    }

    // Retrieve all stored categories stored in database
    function getAllCategories() {
        var categories = new Set();
        var store = getObjectStore(USAGE_DB_STORE_NAME, 'readonly');
        var req = store.index('appName').openCursor();
        req.onsuccess = function (event) {
            var cursor = event.target.result;
            if (cursor) {
                categories.add(cursor.value.appCategory);
                cursor.continue();
            }
            else {
                databaseInfo.categories = Array.from(categories);
                getAllMixedCategories();
            }
        };

        //Error handling
        req.onerror = function (evt) {
            console.error("getAllCategories:", evt.target.errorCode);
        };
    }

    // Retrieve all mixed stored categories stored in database
    function getAllMixedCategories() {
        var mixedCategory = [];
        var store = getObjectStore(MIXED_CATEGORY_STORE_NAME, 'readonly');
        var req = store.index('appName').openCursor();
        req.onsuccess = function (event) {
            var cursor = event.target.result;
            if (cursor) {
                mixedCategory.push(cursor.value);
                cursor.continue();
            }
            else {
                databaseInfo.mixedCategory = mixedCategory;
                getDailyAppUsage();
                getWeeklyAppUsages();
                getCategorisePage();
            }
        };

        //Error handling
        req.onerror = function (evt) {
            console.error("getAllCategories:", evt.target.errorCode);
        };
    }


    //Get a list of categories, their usages and also a list of app information
    function getDailyAppUsage() {
        var categoryList = new Set();
        var categoryTime = []
        var appList = [];

        var store = getObjectStore(USAGE_DB_STORE_NAME, 'readonly');
        var req = store.index('appName').openCursor();
        req.onsuccess = function (event) {
            var cursor = event.target.result;

            // Find the daily usage recorded in the object store
            // by using the index of app names.
            var today = new Date().format('Y-m-d');
            if (cursor) {
                var appName = cursor.value.appName;
                if (cursor.value.date == today) {
                    if (cursor.value.appCategory == 'unrecorded') {
                        cursor.continue();
                    }
                    else if (cursor.value.appCategory == 'mixed') {
                        appList.push([appName, cursor.value.appUsage, cursor.value.appCategory]);
                        cursor.continue();
                    }
                    else {
                        appList.push([appName, cursor.value.appUsage, cursor.value.appCategory]);
                        categoryList.add(cursor.value.appCategory);

                        var index = findIndex(cursor.value.appCategory, categoryList);
                        if (categoryTime[index] === undefined) {
                            categoryTime[index] = 0;
                        }
                        categoryTime[index] += cursor.value.appUsage;
                        cursor.continue();
                    }
                }
                else {
                    cursor.continue();
                }
            }
            else {
                appList.forEach(function (element, index) {
                    if (element[2] == 'mixed') {
                        var appName = element[0];
                        var appUsage = element[1];
                        var appIndex = findIndex(appName, databaseInfo.mixedCategory.map(a => a.appName));
                        appList.splice(index, 1);

                        categoryList.add('highly productive');
                        categoryList.add('productive');
                        categoryList.add('medium');
                        categoryList.add('distractive');
                        categoryList.add('highly distractive');

                        appList.push([appName, databaseInfo.mixedCategory[appIndex].highlyProductive * appUsage / 100, 'highly productive']);
                        appList.push([appName, databaseInfo.mixedCategory[appIndex].productive * appUsage / 100, 'productive']);
                        appList.push([appName, databaseInfo.mixedCategory[appIndex].medium * appUsage / 100, 'medium']);
                        appList.push([appName, databaseInfo.mixedCategory[appIndex].distractive * appUsage / 100, 'distractive']);
                        appList.push([appName, databaseInfo.mixedCategory[appIndex].highlyDistractive * appUsage / 100, 'highly distractive']);

                        var index = findIndex('highly productive', categoryList);
                        if (categoryTime[index] === undefined) {
                            categoryTime[index] = 0;
                        }
                        categoryTime[index] += databaseInfo.mixedCategory[appIndex].highlyProductive * appUsage / 100;

                        index = findIndex('productive', categoryList);
                        if (categoryTime[index] === undefined) {
                            categoryTime[index] = 0;
                        }
                        categoryTime[index] += databaseInfo.mixedCategory[appIndex].productive * appUsage / 100;

                        index = findIndex('medium', categoryList);
                        if (categoryTime[index] === undefined) {
                            categoryTime[index] = 0;
                        }
                        categoryTime[index] += databaseInfo.mixedCategory[appIndex].medium * appUsage / 100;

                        index = findIndex('distractive', categoryList);
                        if (categoryTime[index] === undefined) {
                            categoryTime[index] = 0;
                        }
                        categoryTime[index] += databaseInfo.mixedCategory[appIndex].distractive * appUsage / 100;

                        index = findIndex('highly distractive', categoryList);
                        if (categoryTime[index] === undefined) {
                            categoryTime[index] = 0;
                        }
                        categoryTime[index] += databaseInfo.mixedCategory[appIndex].highlyDistractive * appUsage / 100;
                    }
                });

                databaseInfo.dailyCategories = Array.from(categoryList);
                databaseInfo.dailyCategoryUsages = categoryTime;
                databaseInfo.appList = appList;
                constructDailyReportPage(databaseInfo.dailyCategories, databaseInfo.dailyCategoryUsages, databaseInfo.appList, databaseInfo.mixedCategory);
                getGoalsAndTasks();
            }
        };

        // Error handling
        req.onerror = function (evt) {
            console.error("getDailyAppUsage:", evt.target.errorCode);
        };
    }

    // Clear goal object sotre explicitly
    function clearGoalStore() {
        var store = getObjectStore(GOAL_DB_STORE_NAME, 'readwrite');
        clearStore(store);
    }

    // Get the usage of one specific app
    function getAppUsage(key, store, success_callback) {
        var req = store.get(key);
        req.onsuccess = function (evt) {
            var value = evt.target.result;
            if (value)
                success_callback(value.appUsage);
        };
    }

    function getWeeklyAppUsages() {
        var dates = [];
        var date = new Date();
        for (var i = 0; i < 7; i++) {
            dates.push(date.format('Y-m-d'));
            date.setDate(date.getDate() - 1);
        }

        // Group weekly app usages based on their categories.
        // object in the weeklyUsages {[appUsage, date]}
        var weeklyUsages = [];
        var categoryList = new Set();
        var mixedApp = [];

        var store = getObjectStore(USAGE_DB_STORE_NAME, 'readonly');
        var req = store.index('appName').openCursor();
        req.onsuccess = function (event) {
            var cursor = event.target.result;

            var today = new Date().format('Y-m-d');
            if (cursor) {
                var appName = cursor.value.appName;
                var date = cursor.value.date;
                if (cursor.value.appCategory == 'unrecorded') {
                    cursor.continue();
                }
                else if (cursor.value.appCategory == 'mixed') {
                    mixedApp.push([cursor.value.appName, cursor.value.appUsage, cursor.value.date]);
                    cursor.continue();
                }
                else {
                    categoryList.add(cursor.value.appCategory);

                    var index = findIndex(cursor.value.appCategory, categoryList);
                    if (weeklyUsages[index] === undefined) {
                        weeklyUsages[index] = [];
                    }
                    weeklyUsages[index].push({
                        appUsage: cursor.value.appUsage,
                        date: cursor.value.date
                    });
                    cursor.continue();
                }
            }
            else {
                for (var it of mixedApp) {
                    var appUsage = it[1];
                    var date = it[2];

                    categoryList.add('highly productive');
                    categoryList.add('productive');
                    categoryList.add('medium');
                    categoryList.add('distractive');
                    categoryList.add('highly distractive');

                    var appIndex = findIndex(it[0], databaseInfo.mixedCategory.map(a => a.appName));

                    var index = findIndex('highly productive', categoryList);
                    if (weeklyUsages[index] === undefined) {
                        weeklyUsages[index] = [];
                    }
                    weeklyUsages[index].push({
                        appUsage: databaseInfo.mixedCategory[appIndex].highlyProductive * appUsage / 100,
                        date: date
                    });

                    index = findIndex('productive', categoryList);
                    if (weeklyUsages[index] === undefined) {
                        weeklyUsages[index] = [];
                    }
                    weeklyUsages[index].push({
                        appUsage: databaseInfo.mixedCategory[appIndex].productive * appUsage / 100,
                        date: date
                    });

                    index = findIndex('medium', categoryList);
                    if (weeklyUsages[index] === undefined) {
                        weeklyUsages[index] = [];
                    }
                    weeklyUsages[index].push({
                        appUsage: databaseInfo.mixedCategory[appIndex].medium * appUsage / 100,
                        date: date
                    });

                    index = findIndex('distractive', categoryList);
                    if (weeklyUsages[index] === undefined) {
                        weeklyUsages[index] = [];
                    }
                    weeklyUsages[index].push({
                        appUsage: databaseInfo.mixedCategory[appIndex].distractive * appUsage / 100,
                        date: date
                    });

                    index = findIndex('highly distractive', categoryList);
                    if (weeklyUsages[index] === undefined) {
                        weeklyUsages[index] = [];
                    }
                    weeklyUsages[index].push({
                        appUsage: databaseInfo.mixedCategory[appIndex].highlyDistractive * appUsage / 100,
                        date: date
                    });
                }
                console.log(categoryList);
                console.log(weeklyUsages);
                databaseInfo.weeklyCategories = Array.from(categoryList);
                databaseInfo.weeklyUsages = weeklyUsages;
                databaseInfo.date = dates;
                constructWeeklyReportPage(databaseInfo.weeklyCategories, databaseInfo.date, databaseInfo.weeklyUsages);
            }
        };

        //Error handling
        req.onerror = function (evt) {
            console.error("generateWeeklyReport:", evt.target.errorCode);
        };
    }

    // Retrive all goals from the indexedDb using the index of goal names
    function getGoalsAndTasks() {
        var store = getObjectStore(GOAL_DB_STORE_NAME, 'readonly');
        var req = store.index('goalName').openCursor();

        var currentGoal;
        var goalName = new Set();
        //related tasks (by index)
        var goalDataSets = [];

        req.onsuccess = function (event) {
            var cursor = event.target.result;

            if (cursor) {
                goalName.add(cursor.value.goalName);

                var i = 0;
                for (var it of goalName) {
                    if (cursor.value.goalName == it) {
                        if (cursor.value.currentGoal == true) {
                            currentGoal = i;
                        }
                        if (goalDataSets[i] === undefined) {
                            goalDataSets[i] = [];
                        }

                        goalDataSets[i].push({
                            description: cursor.value.description,
                            noMoreThan: cursor.value.noMoreThan,
                            duration: cursor.value.duration
                        });
                        break;
                    }
                    i++;
                }
                cursor.continue();

            }
            else {
                databaseInfo.goals = Array.from(goalName);
                databaseInfo.tasks = goalDataSets;
                databaseInfo.currentGoal = currentGoal;

                constructGoalPage(databaseInfo.goals);
                constructGoalAddingPage(databaseInfo.categories, databaseInfo.goals, databaseInfo.tasks);
                constructHomePage((databaseInfo.currentGoal === undefined ? undefined : databaseInfo.goals[currentGoal]), databaseInfo.dailyCategories, databaseInfo.dailyCategoryUsages, (databaseInfo.currentGoal === undefined ? undefined : databaseInfo.tasks[currentGoal]));

            }
        };

        req.onerror = function (evt) {
            console.error("generateGoal:", evt.target.errorCode);
        };
    }

    function getCategorisePage() {
        var appName = new Set();
        var category = new Array();

        // Iterate the whole database
        var store = getObjectStore(USAGE_DB_STORE_NAME, 'readonly');
        var req = store.index('appName').openCursor();

        req.onsuccess = function (event) {
            var cursor = event.target.result;

            if (cursor) {
                if (!appName.has(cursor.value.appName)) {
                    appName.add(cursor.value.appName);
                    category.push([cursor.value.appName, cursor.value.appCategory]);
                }

                cursor.continue();
            }
            else {
                databaseInfo.allAppWithCategories = category;
                constructCategoryPage(databaseInfo.allAppWithCategories);
            }

        }

        // Error handling
        req.onerror = function (evt) {
            console.error("getCategorisePage:", evt.target.errorCode);
        };
    }

    // Set the goal that has been selected as the current goal.
    function setCurrentGoal() {
        var list = $("#goal-list");
        if (list.length) {
            var goalName = list.find(":selected").text();
            var store = getObjectStore(GOAL_DB_STORE_NAME, 'readwrite');
            var req = store.index('goalName').openCursor();

            req.onsuccess = function (event) {
                var cursor = event.target.result;
                if (cursor) {
                    var obj = cursor.value;
                    obj.currentGoal = (obj.goalName == goalName ? true : false);

                    cursor.update(obj);
                    cursor.continue();
                }
                else {
                    buildPages();
                    console.log("Finish setting the current goal.");
                }
            };

            req.onerror = function (evt) {
                console.error("setCurrentGoal:", evt.target.errorCode);
            };


        }
    }


	/**
	 * Add app info
	 *
	 * @param {string} appName
	 * @param {string} appCategory
	 * @param {number} appUsage
	 * @param {date=} date
	 */
    function addApp(appName, appCategory, appUsage, date, store) {
        console.log("addApp arguments:", arguments);
        var obj = { appName: appName.toLowerCase(), appCategory: appCategory.toLowerCase(), appUsage: appUsage };
        if (typeof date != 'undefined')
            obj.date = date;

        if (store === undefined) {
            store = getObjectStore(USAGE_DB_STORE_NAME, 'readwrite');
        }

        var req;
        try {
            req = store.add(obj);
        } catch (e) {
            if (e.name == 'DataCloneError')
                alert("This engine doesn't know how to clone a Info, " +
                    "use Firefox");
            throw e;
        }
        req.onsuccess = function (evt) {
            console.log("Insertion in USAGE DB successful");
            buildPages();
        };
        req.onerror = function () {
            console.error("addApp error", this.error);
            // alert(this.error);
        };
    }

	/**
	 * Add goal info
	 *
	 * @param {string} goalName
	 */
    function addGoal(goalName, description, noMoreThan, duration) {
        console.log("addGoal arguments:", arguments);
        var obj = { goalName: goalName, description: description, noMoreThan: noMoreThan, duration: duration, currentGoal: false };

        var store = getObjectStore(GOAL_DB_STORE_NAME, 'readwrite');
        var req;
        try {
            req = store.add(obj);
        } catch (e) {
            if (e.name == 'DataCloneError')
                alert("This engine doesn't know how to clone a Blob, " +
                    "use Firefox");
            throw e;
        }
        req.onsuccess = function (evt) {
            console.log("Insertion in GOAL DB successful");
            alert("Insertion in GOAL DB successful");
        };
        req.onerror = function () {
            console.error("addGoal error", this.error);
            alert(this.error);
        };
    }

	/**
	 * @param {string} appName
	 */
    function deleteMixedCategory(appName) {
        console.log("delete mixed category:", arguments);
        var store = getObjectStore(MIXED_CATEGORY_STORE_NAME, 'readwrite');
        var req = store.get(appName);
        req.onsuccess = function (evt) {
            if (typeof evt.target.result == 'undefined') {
                return;
            }
            req = store.delete(appName);
            req.onsuccess = function (evt) {
                console.log("evt:", evt);
                console.log("evt.target:", evt.target);
                console.log("evt.target.result:", evt.target.result);
                console.log("delete successful");
                alert("Deletion successful");
                buildPages();
            };
            req.onerror = function (evt) {
                console.error("deleteData:", evt.target.errorCode);
            };
        };
    }


	/**
	 * Delete the goal using its name
	 *
	 * @param {string} goalName
	 */
    function deleteGoal(goalName) {
        console.log("delete goal:", arguments);

        var store = getObjectStore(GOAL_DB_STORE_NAME, 'readwrite');
        var req = store.index('goalName');
        req.get(goalName).onsuccess = function (evt) {
            if (typeof evt.target.result == 'undefined') {
                alert("No matching record found");
                return;
            }
            deleteData(evt.target.result.id, store);
        };
        req.onerror = function (evt) {
            console.error("deleteGoal:", evt.target.errorCode);
        };
    }

	/**
	 * Delete some data with its key and its store
	 *
	 * @param {number} key
	 * @param {IDBObjectStore=} store
	 */
    function deleteData(key, store) {
        var req = store.get(key);
        req.onsuccess = function (evt) {
            var record = evt.target.result;

            if (typeof record == 'undefined') {
                alert("No matching record found");
                return;
            }

            req = store.delete(key);
            req.onsuccess = function (evt) {
                alert("Deletion successful");
                buildPages();
            };
            req.onerror = function (evt) {
                console.error("deleteData:", evt.target.errorCode);
            };
        };
        req.onerror = function (evt) {
            console.error("deleteData:", evt.target.errorCode);
        };
    }

	/**
	 * Update the app category
	 *
	 * @param {string} appName
	 * @param {number} appUsage
	 * @param {string} appDate Y-m-d
	 */
    function updateAppCategory(store, appName, appCategory) {
        let req;
        store.openCursor().onsuccess = function (evt) {
            var cursor = evt.target.result;
            // If the cursor is pointing at something, ask for the data
            if (cursor) {
                if (cursor.value.appName == appName.toLowerCase()) {
                    var obj = cursor.value;
                    if (obj.appCategory == 'mixed') {
                        console.log("used to be mixed");
                        deleteMixedCategory(cursor.value.appName);
                    }
                    obj.appCategory = appCategory.toLowerCase();

                    req = cursor.update(obj);
                    req.onsuccess = function () {
                        console.log("Update info! Request is: ", req, " ", "appName: ", appName, " appCategory: ", appCategory);
                    };
                }
                cursor.continue();
            }
            else {
                //buildPages();
            }
        };
    }

	/**
	 * Update the app usage information
	 *
	 * @param {string} appName
	 * @param {number} appUsage
	 * @param {string} appDate Y-m-d
	 */
    function updateApp(store, appName, appUsage, appDate) {
        let req;
        store.openCursor().onsuccess = function (evt) {
            var cursor = evt.target.result;
            // If the cursor is pointing at something, ask for the data
            if (cursor) {
                if (cursor.value.appName == appName.toLowerCase() && cursor.value.date == appDate) {
                    var obj = cursor.value;
                    obj.appUsage = appUsage;

                    req = cursor.update(obj);
                    req.onsuccess = function () {
                        console.log("Update info! Request is: ", req, " ", "appName: ", appName, " appUsage: ", appUsage);
                    };
                }
                cursor.continue();
            }
            else {
                if (req === undefined) {
                    addApp(appName, "Other", appUsage, appDate, store);
                }
                // buildPages();
            }
        };
    }

	/**
	 * Add new data into DB,
	 * if exists, update it
	 *
	 * @param {string} appName
	 * @param {int array} inputs
	 */
    function addMixedCategoryToDB(appName, inputs) {
        // Data entered by users
        var obj = {
            appName: appName.toLowerCase(),
            highlyProductive: inputs[0],
            productive: inputs[1],
            medium: inputs[2],
            distractive: inputs[3],
            highlyDistractive: inputs[4]
        }

        let req = undefined;
        let store = getObjectStore(MIXED_CATEGORY_STORE_NAME, 'readwrite');

        // Iterate over the DB
        store.openCursor().onsuccess = function (evt) {
            var cursor = evt.target.result;

            if (cursor) { // If the cursor is pointing at something, ask for the data

                if (cursor.value.appName == appName.toLowerCase()) {
                    // If data exists
                    let obj = cursor.value;

                    obj.highlyProductive = inputs[0];
                    obj.productive = inputs[1];
                    obj.medium = inputs[2];
                    obj.distractive = inputsf[3];
                    obj.highlyDistractive = inputs[4];

                    req = cursor.update(obj);
                    req.onsuccess = function () {
                        console.log("Update mixed category ratio");
                    };
                }

                cursor.continue();
            }
            else { // If new data is entered
                if (req === undefined) {

                    req = store.add(obj);

                    req.onsuccess = function (evt) {
                        console.log("Insertion in mixed category successful");
                    };

                    req.onerror = function () {
                        console.error("addMixedCategory error", this.error);
                    };
                }
            }
        }
    }

	/**
	 *
	 * @param {string} appName
	 * @param {int array} inputs
	 */
    function setMixedCategory(appName, inputs) {
        // Calculate the total ratio, it has to be 100.
        // Otherwise, each category will be set to 20.
        var total = 0;
        for (var it of inputs) {
            total += it;
        }

        if (total != 100) {
            alert("The sum of each ratio should be 100. Set each ratio to 20 by default");
            for (var i = 0; i < inputs.length; i++) {
                inputs[i] = 20;
            }
        }

        // Add data to DB
        addMixedCategoryToDB(appName, inputs);

        // Close the popup
        closeMixedCategoryPopup(true);
    }

	/**
	 * Hide the popup table
	 *
	 * @param {boolean} success if true, the app category will be changed to mixed,
	 * 							else remains the same.
	 */
    function closeMixedCategoryPopup(success = false) {
        let popup = $('#mixed-category-ratio');

        // If users confirm the ratio
        if (success) {
            var store = getObjectStore(USAGE_DB_STORE_NAME, 'readwrite');
            updateAppCategory(store, popup.find('#mixed-category-app-name').text(), 'mixed');
        }

        // Empty input field
        popup.hide();
        popup.find('.required').val('');

    }

    // Setup native messaging for native app and extension
    function parseNativeAppMessage(msg) {
        if (msg.appName.length != msg.appUsage.length) {
            console.log("invalid message from native app");
            return;
        }
        var store = getObjectStore(USAGE_DB_STORE_NAME, 'readwrite');
        var date = new Date().format('Y-m-d');

        for (var i = 0; i < msg.appName.length; i++) {
            updateApp(store, msg.appName[i], msg.appUsage[i], date);
        }
    }

    // Receive the messsage from the native app
    function receiveMessage(request, sender, sendResponse) {
        if (request.msg.localeCompare("native app") == 0) { // get updated data from native app
            parseNativeAppMessage(request);
        }
    }

    // Setup event listeners
    function addEventListeners() {
        console.log("addEventListeners");

        $('#add-button').click(function (evt) {
            console.log("add app ...");
            var appName = $('#app-name').val();
            var appCategory = $('#app-category').val();
            if (!appName || !appCategory) {
                alert("Required field(s) missing");
                return;
            }
            var appUsage = $('#app-usage').val();
            if (appUsage != '') {
                // Better use Number.isInteger if the engine has EcmaScript 6
                if (isNaN(appUsage)) {
                    alert("Invalid app usage");
                    return;
                }
                appUsage = Number(appUsage);
            } else {
                appUsage = null;
            }
            var date = $("#date-record").val();
            addApp(appName, appCategory, appUsage, date);
        });

        $('#add-task').click(function (evt) {
            console.log("add goal ...");

            var goalName;
            var goalIndex;
            if ($('#entered-goal-name').is(":visible")) {
                goalIndex = databaseInfo.goals.length;
                goalName = $('#entered-goal-name').val();
            }
            else {
                if ($('#goal-list').val() != null) {
                    goalIndex = $('#goal-list').find(":selected").val();
                    goalName = $('#goal-list').find(":selected").text();
                }
            }

            if ($('#category-list').val() != null) {
                var description = $('#category-list').find(":selected").text();
            }

            var noMoreThan = $('#lt').is(':checked');

            var duration = $('#duration-list').val();

            if (isNaN(duration)) {
                console.log("Invalid duration");
                return;
            }

            duration = Number(duration);

            //Error checking
            if (!goalName | !description | !duration) {
                alert("Required field(s) missing");
                console.log("Required field(s) missing");
                return;
            }

            if (!(databaseInfo.tasks[goalIndex] === undefined)) {
                for (var task of databaseInfo.tasks[goalIndex]) {
                    if (task.description == description) {
                        alert("Task conflict!");
                        return;
                    }
                }
            }

            addGoal(goalName.toLowerCase(), description.toLowerCase(), noMoreThan, duration * 60 * 60);

            //Refresh elements in goal page
            if (databaseInfo.tasks[goalIndex] === undefined) {
                databaseInfo.goals.push(goalName);
                databaseInfo.tasks[goalIndex] = [];
                generateOption('goal-list', databaseInfo.goals);
                $("#new-goal").hide();
                $(`#goal-list option[value='${goalIndex}']`).prop('selected', true);
            }

            databaseInfo.tasks[goalIndex].push({ description: description.toLowerCase(), noMoreThan: noMoreThan, duration: duration * 60 * 60 });
            refreshTasks(databaseInfo.tasks, goalIndex);

            //Reset page
            $('#lt').prop('checked', true);
            $('#rt').prop('checked', false);
            $(`#duration-list option[value='${-1}']`).prop('selected', true);
            $(`#category-list option[value='${-1}']`).prop('selected', true);
        });

        $('#delete-button').click(function (evt) {
            console.log("delete ...");
            var appName = $('#app-category-to-delete').val();

            if (appName != '') {
                deleteApp(appName);
            }
        });

        $('#goal-list').on('change', function () {
            console.log("changing...");
            var enterNewGoal = $("#new-goal");
            var tasks = $("#goal-tasks")

            //if need to enter a new goal
            if (databaseInfo.goals.length == Number(this.value)) {
                enterNewGoal.show();
                $("#goal-tasks").empty();
            }

            else {
                if (enterNewGoal.length) {
                    enterNewGoal.hide();
                }

                if (databaseInfo.tasks.length) {
                    refreshTasks(databaseInfo.tasks, Number(this.value));
                }
            }
        });

        // Dynamically add click event to category table
        $('#category-table').on('change click', '.modify-category', function () {
            // Get the current row
            var currentRow = $(this).closest("tr");

            // Get each column
            var appName = currentRow.find("td:eq(0)").text();
            var category = currentRow.find("td:eq(1)").find(":selected").text();

            if (currentRow.find("td:eq(1)").find(":selected").val == -1) {
                alert("Required field(s) missing");
                console.log("Required field(s) missing");
                return;
            }


            if (category == 'mixed') {
                // If mixed category is chosen, popup table is shown
                let popup = $('#mixed-category-ratio');

                if (popup.length) {
                    var temp = popup.find('h2');
                    temp.empty();
                    temp.append(appName);

                    popup.show();
                }
                else {
                    alert("In database.js addEventListeners(), cannot find popup window");
                }
            }
            else {
                // Save changes into DB
                var store = getObjectStore(USAGE_DB_STORE_NAME, 'readwrite');
                updateAppCategory(store, appName, category);
            }
        });

        // When users click "confirm" button in popup table
        $('#mixed-category-confirm').click(function (evt) {
            var inputs = [$('#entered-highly-productive'),
            $('#entered-productive'),
            $('#entered-medium'),
            $('#entered-distractive'),
            $('#entered-highly-distractive')];

            setMixedCategory($('#mixed-category-ratio').find('h2').text(), inputs.map(a => Number(a.val())));
        });

        // When "close" button in popup table is pressed,
        // disgard chanegs including changing category to "mixed"
        $('#popup-close-button').click(function () {
            closeMixedCategoryPopup(false);
        });

        $('#set-current-goal').click(function (evt) {
            setCurrentGoal();
        });

        $('#clear-store-button').click(function (evt) {
            clearGoalStore();
            clearUsageStore();
        });


    }

    openDb();
    addEventListeners();
    chrome.runtime.onMessage.addListener(receiveMessage);


}); // Immediately-Invoked Function Expression (IIFE)

function test() {

}
