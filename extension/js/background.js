console.log('background running');

var port = null;
connectToNativeHost();

// Receive message from other js
chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        console.log("background recieved message from " + sender.url + JSON.stringify(request));
        parseMessage(request);
    }
);

// Ask local app for app usage update per sec
requestUpdate();

/************************
 *                      *
 *  Native messaging    *
 *                      *
 ************************/

//onNativeDisconnect
function onDisconnected() {
    console.log(chrome.runtime.lastError);
    console.log('disconnected from native app.');
    port = null;
}

// Receive message from native app
function onNativeMessage(message) {
    console.log('recieved message from native app: ' + JSON.stringify(message));
    parseMessage(message);
    requestUpdate();
}

//connect to native host and get the communicatetion port
function connectToNativeHost() {
    var nativeHostName = 'com.group_project.time_tracker';
    port = chrome.runtime.connectNative(nativeHostName);
    port.onMessage.addListener(onNativeMessage);
    port.onDisconnect.addListener(onDisconnected);
    console.log("connected");
}

// Send message to native app
function sendMessage(message) {
    if (port != null) {
        port.postMessage(message);
        console.log('send messsage to native app: ' + JSON.stringify(message));
    }
    else {
        console.log("port is unavailable now");
    }
}

function quitApp() {
    sendMessage({ command: "quit", args: "null" });
}

// Parse message from page js
function parseMessage(msg) {
    console.log(msg);

    if (msg.msg.localeCompare("get current goal") == 0) {

    }
    else if (msg.msg.localeCompare("get daily report") == 0) {

    }
    else if (msg.msg.localeCompare("disconnect") == 0) {

        port.disconnect();
        port = null;
    }
    else if (msg.msg.localeCompare("native app") == 0) {

        updateDbAppUsage(msg);
    }
}

// Send message to local app to update info
function requestUpdate() {
    sendMessage({ command: "get app usage", args: "null" });
}

/**************************
 *                        *
 * Communication with DB  *
 *                        *
 **************************/

// Update app usage in database
function updateDbAppUsage(msg) {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        // Only send message when tab is available
        if (!(tabs === undefined) && tabs.length != 0) {
            console.log("send message to database.js");
            chrome.tabs.sendMessage(tabs[0].id, msg);
        }
    });
}
