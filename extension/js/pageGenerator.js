/**
  * This content js is used to generate pages which need communicate with
  * IndexDB
  */


/**
  * @param {Array} categories contains names of all categories
  * @param {Array} categoryUsage contains usage time for each category
  * @param {APPLIST=} appList a list of app information with appName, appCategory and appUsage
  */
function constructDailyReportPage(categories, categoryUsage, appList, mixedCategoryApp) {
    var dataset = {
        label: categories,
        data: categoryUsage,
        backgroundColor: colorGenerator('background', categories.length),
        borderColor: colorGenerator('border', categories.length)
    }

    for (var app of appList) {
        app[1] = formatTime(app[1]);
        for (var mixedApp of mixedCategoryApp) {
            if (app[0] == mixedApp.appName) {
                if (app[2] == 'highly productive') {
                    app[2] += ('(' + mixedApp.highlyProductive + '%)');
                }
                if (app[2] == 'productive') {
                    app[2] += ('(' + mixedApp.productive + '%)');
                }
                if (app[2] == 'medium') {
                    app[2] += ('(' + mixedApp.medium + '%)');
                }
                if (app[2] == 'distractive') {
                    app[2] += ('(' + mixedApp.distractive + '%)');
                }
                if (app[2] == 'highly distractive') {
                    app[2] += ('(' + mixedApp.highlyDistractive + '%)');
                }
            }
        }
    }

    drawChart('dailychart', 'doughnut', categories, new Array(dataset), 'Daily Usage Report');
    generateTable('daily-table', ['App Name', 'Usage', 'App Category'], appList);

}

/**
 * @param {Array} categories contains names of all categories
 * @param {Array} date contains string name of last seven days
 * @param {WEEKLYUSAGES=} weeklyUsages a list of weekly usage information: [appUge, date]
 */
function constructWeeklyReportPage(categories, date, weeklyUsages) {
    var datasets = [];
    datasets.length = weeklyUsages.length;

    for (var i = 0; i < weeklyUsages.length; i++) {
        var data = []
        data.length = 7;
        for (var j of weeklyUsages[i]) {
            for (var m = 0; m < 7; m++) {
                if (data[m] === undefined) {
                    data[m] = 0;
                }
                if (date[m] == j.date) {
                    data[m] += j.appUsage;
                }
            }
        }

        datasets[i] = {
            label: categories[i],
            data: data,
            backgroundColor: colorGenerator('background', weeklyUsages.length)[i],
            borderColor: colorGenerator('border', weeklyUsages.length)[i]
        };
    }

    var thead = [...date];
    thead.unshift('App Category');
    var tbody = [];
    for (var it of datasets) {
        var row = [...it.data.map(a => formatTime(a))];
        row.unshift(it.label);
        tbody.push(row);
    }


    drawChart('weeklychart', 'horizontalBar', date, datasets, 'Weekly Usage Report');
    generateTable('weekly-table', thead, tbody);
}

function constructGoalAddingPage(categories, goals) {
    generateOption('category-list', ['highly productive', 'productive', 'medium', 'distractive', 'highly distractive']);

    var enterNewGoal = $("#new-goal");
    if (enterNewGoal.length) {
        enterNewGoal.hide();
        $('#goal-list').append("<option value = " + goals.length + "> Create a new goal </option>")
    }


}

function constructGoalPage(goals) {
    generateOption('goal-list', goals);
}

/**
 * Using DB to construct "categorise" page
 *
 * @param {array of array} allAppWithCategories contains array of [appName, category]
 */
function constructCategoryPage(allAppWithCategories) {

    var allCategories = ['highly productive', 'productive', 'medium', 'distractive',
        'highly distractive', 'mixed', 'others', 'unrecorded'];
    var dropDownBox = allAppWithCategories.slice();

    dropDownBox.forEach(function (value, index) {
        // value: [appName, appCategory]
        if (value.length != 2) {
            alert('In constructCategoryPage(), wrong parameter allAppWithCategories');
            return;
        }

        // Generate dropdown button for each app
        dropDownBox[index][1] = generateDropdown(value[0], 'modify-category', allCategories, value[1]);
    });

    generateTable('category-table', ['App Name', 'App Category'], dropDownBox);
}

function refreshTasks(tasks, value) {
    generateTaskList(tasks[value]);
}

function constructHomePage(currentGoal, categories, categoryUsages, tasks) {
    var goalCategory = []
    var goalName = $("#current-goal-name");
    var goalTask = $("#current-goal-task");
    if (goalName.length) {
        goalName.empty();
        goalTask.empty();
        if (currentGoal === undefined) {
            goalName.append('current goal: <span style="color:#A9A9A9">Current goal is not setted.</span>');
        }
        else {
            goalName.append('current goal: <span style="color:#DEB887">' + currentGoal + '</span>');
            tasks.forEach(function (task, index) {
                var percent = 0;
                var isAchieved = false;

                for (var i = 0; i < categories.length; i++) {
                    if (task.description == categories[i]) {
                        goalCategory[index] = (task.noMoreThan ? -categoryUsages[i] : categoryUsages[i]);
                        percent = categoryUsages[i] / task.duration;
                        if ((percent >= 1 && task.noMoreThan == false) || (percent <= 1 && task.noMoreThan == true))
                            isAchieved = true;
                        break;
                    }
                }

                if (percent == 0) {
                    if (task.noMoreThan == true) {
                        isAchieved = true;
                    }
                }

                var htmlDescription = '<li><span class="tab" style = "color: ' + (isAchieved ? '#228B22' : '#808080') + '">'
                    + task.description + (task.noMoreThan ? ' less than ' : ' greater than ') + (task.duration / 3600).toFixed(0) + ' hour(s) </span></a>'
                    + '<progress value = "' + percent + '" max = "1"></progress><span>' + (percent * 100).toFixed(2) + '% </span></li>';
                goalTask.append(htmlDescription);
            });

            var datasets = [];
            datasets.push({
                label: "current usages",
                data: goalCategory.map(a => a / 3600),
                backgroundColor: colorGenerator('background', goalCategory.length),
                borderColor: colorGenerator('border', goalCategory.length)
            });

            datasets.push({
                label: "goal",
                data: tasks.map(a => (a.noMoreThan ? -a.duration / 3600 : a.duration / 3600)),
                backgroundColor: colorGenerator('background', tasks.length)[tasks.length - 1],
                borderColor: colorGenerator('border', tasks.length)[tasks.length - 1],
                type: 'line',
                pointRadius: 5,
                pointHoverRadius: 10,
                showLine: false
            });

            drawChart('combo-chart', 'bar', tasks.map(a => a.description), datasets, 'Daily Usages');
        }
    }
}

function generateTaskList(tasks) {
    if ($("#goal-tasks").length) {
        $("#goal-tasks").empty();
        for (var it of tasks) {
            $("#goal-tasks").append('<li><span class="tab">' + it.description + (it.noMoreThan ? ' less than ' : ' greater than ') + (it.duration / 3600).toFixed(0) + ' hour(s) </span></a></li>');
        }
    }
}

function generateOption(id, texts, optionText) {
    var options = $('#' + id);

    if (options.length) {
        options.empty();
        if (id == 'goal-list') {
            options.append('<option disabled="disabled" selected="selected" value="-1">Select a Goal</option>');
        }
        else if (id == 'category-list') {
            options.append('<option disabled="disabled" selected="selected" value="-1">Select a Category</option>');
        }

        texts.forEach(function (element, index) {
            options.append("<option value = " + index + ">" + element + "</option>");
        });
    }
}

/**
 *
 * @param {any} id id for the select tag
 * @param {string} className class name for the select tag
 * @param {string} texts options content
 * @param {string} defaultText default content to show
 */
function generateDropdown(id, className, texts, defaultText) {

    var button = '<select ' + 'id=' + id + '-dropdown-button class=' + className + '>';

    // Default content
    button += '<option disabled="disabled" selected="selected" value="-1">' + defaultText + '</option>';

    texts.forEach(function (element) {
        //if (element == defaultText) continue;
        button += "<option value = " + element + ">" + element + "</option>";
    });

    button += '</select>';

    return button;
}

function generateTable(id, thead, tbody) {
    var table = $('#' + id);

    if (table.length) {
        table.empty();
        var temp = '<thead><tr>';
        for (var it of thead) {
            temp += '<th>' + it + '</th>';
        }
        temp += '</tr></thead><tbody>';

        for (var it of tbody) {
            temp += '<tr>';
            for (var sec_it of it) {
                temp += '<td>' + sec_it + '</td>';
            }
            temp += '</tr>';
        }

        temp += '</tbody>';
    }

    table.append(temp);
    table.dynatable();

}

function drawChart(id, type, labels, datasets, title) {
    var ctx;
    let tag = '#' + id;
    if ($(tag).length) {
        ctx = $(tag).get(0).getContext('2d');
        var myChart = new Chart(ctx, {
            type: type,
            data: {
                labels: labels,
                datasets: datasets
            },
            options: {
                title: {
                    display: true,
                    text: title
                }
            }
        });
    }
}

/**
 * Generate a list of colors
 *
 * @param {string} mode background or borderColor
 * @param {number} length specify the length of color array
 */
function colorGenerator(mode, length) {
    var background = [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
    ];
    var border = [
        'rgba(255, 99, 132, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
    ]

    var color = []

    for (var i = 0; i < length; i++) {
        if (mode == 'background')
            color.push(background[i % 6]);
        if (mode == 'border')
            color.push(border[i % 6]);
    }

    return color;
}

function findIndex(target, list) {
    var index = 0;
    for (var it of list) {
        if (target == it) {
            return index;
        }
        index++;
    }
    return -1;
}

function formatTime(seconds) {
    var hours = Math.floor(seconds / 3600);
    seconds -= 3600 * hours;
    var minutes = Math.floor(seconds / 60);
    seconds -= 60 * minutes;

    return (hours ? hours + ' h ' : '') + (minutes ? minutes + ' min ' : '') + ((hours || minutes) ? '' : seconds + ' s');
}
