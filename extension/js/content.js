$(function () {
    // When home button is pressed, go to the page
    // and request "current_goal" from DB
    $('#dashbord').click(function (event) {
        window.location.href = "dashboard.html";
        chrome.runtime.sendMessage({ msg: "get current goal" });
        console.log("send (get current goal) to background");
    });

    // When report button is pressed, go to the page
    // and request "daily_report" from DB
    $('#daily_report').click(function (event) {
        window.location.href = "dailyreport.html";
        chrome.runtime.sendMessage({ msg: "get daily report" });
        console.log("send (get daily report) to background");
    });

    $('#back-to-goal').click(function (event) {
        window.location.href = "goal.html";
    });
});
